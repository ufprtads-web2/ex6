/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ufpr.tads.web2.facade;

import com.ufpr.tads.web2.beans.Cliente;
import com.ufpr.tads.web2.dao.ClienteDAO;
import com.ufpr.tads.web2.servlets.ClientesServlet;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author cassiano
 */
public class ClientesFacade {
    
    static ClienteDAO dao = null;
    static Cliente cliente = null;
    public static List listClients() throws SQLException{
        dao = new ClienteDAO();
        cliente = new Cliente();
        List<Cliente> resultado = null;                    
        try {
            resultado = dao.selectClientes();            
        } catch (SQLException ex) {
            Logger.getLogger(ClientesFacade.class.getName()).log(Level.SEVERE, null, ex);
        }
        finally {
            dao.closeConnection();                       
        }
        return resultado;
    }
    public static Cliente getClient(String id) throws SQLException{        
        cliente = null;
        if (id != null){
            dao = new ClienteDAO();        
            try {                                    
                cliente = dao.selectCliente(id);
            } catch (SQLException ex) {
                Logger.getLogger(ClientesFacade.class.getName()).log(Level.SEVERE, null, ex);
            }
            finally {
            dao.closeConnection();
            }            
        }
        return cliente;        
    }
    public static void deleteClient(String id) throws SQLException{        
        if (id != null){                       
            dao = new ClienteDAO();
            try {
                dao.removeCliente(id);            
            } catch (SQLException ex) {
                Logger.getLogger(ClientesFacade.class.getName()).log(Level.SEVERE, null, ex);
            }
            finally {
                dao.closeConnection();
            }        
        }
    }
    
    public static void updateClient(Cliente c) throws SQLException{          
        if (cliente != null){
            try {
                dao = new ClienteDAO();
                dao.updateCliente(c);            
            } catch (SQLException ex) {
                Logger.getLogger(ClientesFacade.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                dao.closeConnection();
            }            
        }
    }
    
    public static void addClient(Cliente cli) throws SQLException{
//        if (cli != null){
        try {            
            dao = new ClienteDAO();
            dao.insertCliente(cli);
        } catch (SQLException ex) {
            Logger.getLogger(ClientesFacade.class.getName()).log(Level.SEVERE, null, ex);
        }finally {
            dao.closeConnection();
        }                
    }
}
